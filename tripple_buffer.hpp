//Copyright 2017-2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#pragma once

#include <iterator>

namespace algorithm
{
	template<typename held_t, typename mutex_t, bool _contiguous>
	class internal_tripple_buffer
	{
		public:
			constexpr static bool contiguous = _contiguous;
			/*!
			 * Pushes the in buffer into the middle buffer
			 *
			 * DOES clear the in buffer contents. 
			 *
			 * Completely threadsafe
			 */ 
			inline void push_in_to_middle()
			{
				auto _ = std::lock_guard(accessing_out_mutex);
				auto __ = std::lock_guard(accessing_middle_mutex);
				if constexpr(contiguous)
				{
					middle.insert(std::end(middle),std::begin(in),std::end(in));
					in = held_t();
				}
				else
				{
					std::swap(middle,in);
					in.clear();
					has_data_in_middle = true;
				}
			}	
			/*!
			 * Pushes the middle buffer into the out buffer
			 *
			 * If contigous:
			 *
			 * Clears middle buffer contents.
			 *
			 * If not:
			 *
			 * Does NOT clear the middle contents -- so if they havn't been updated since the last push_middle_to_out call, they will be stale but not blank.
			 *
			 * Completely threadsafe
			 */
			inline void push_middle_to_out()
			{
				auto _ = std::lock_guard(accessing_out_mutex);
				auto __ = std::lock_guard(accessing_middle_mutex);
				if constexpr(contiguous)
				{
					std::swap(out,middle);
					middle.clear();
				}
				else if (has_data_in_middle)
				{
					std::swap(out,middle);
					has_data_in_middle = false;
				}
			}

			held_t in;
			mutex_t accessing_in_mutex;

		private:
			held_t middle;
			mutex_t accessing_middle_mutex;
			//Only used when non-contigous
			[[no_unique_address]] bool has_data_in_middle = false;
		public:
			//Writing to out is undefined
			held_t out;
			mutex_t accessing_out_mutex;

	};
	template<typename held_t, typename mutex_t>
	using tripple_buffer = internal_tripple_buffer<held_t,mutex_t,false>;

	template<typename held_t, typename mutex_t>
	using tripple_buffer_contiguous = internal_tripple_buffer<held_t,mutex_t,true>;
}
