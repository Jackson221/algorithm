/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Container algorithms
 */
#pragma once

#include <functional>
#include <type_traits>
#include <set>
#include <iterator>
#include <map>
#include <vector>

#include "algorithm/type_traits.hpp"


namespace algorithm
{

	/*!
	 * Iterates through a STL-compatible container and only runs run_function on types which unique_id_generator gives the same ID value for.
	 *
	 * If unspecified, unique_id_generator will simply output the data at the iterator passed into it.
	 */
	template <class iterator, typename unique_id_generator_t>
	inline void for_each_unique(iterator start, iterator end, std::function<void(typename iterator::value_type)> run_function,unique_id_generator_t unique_id_generator)
	{
		using ID_t = std::result_of_t<decltype(unique_id_generator)(iterator)>;
		auto seen_IDs = std::set<ID_t>();//std::distance(start,end));

		for (auto it = start;it != end;it++)
		{
			ID_t this_id = unique_id_generator(it);
			auto id_position = seen_IDs.find(this_id);
			if (id_position == seen_IDs.end())
			{
				seen_IDs.insert(id_position,this_id);
				run_function(*it);
			}
		}
	}
	template<class iterator>
	inline void for_each_unique(iterator start, iterator end,std::function<void(typename iterator::value_type)> run_function )
	{
		for_each_unique(start, end, run_function, [](iterator in){return *in;});
	}


	template<class new_container_t, class container, class ... more_t>
	inline void insert_all_into_container(const new_container_t& new_container, container to_be_inserted)
	{
		new_container.insert(new_container.end(),to_be_inserted.begin(),to_be_inserted.end());	
	}
	
	template<class new_container_t, class container, class ... more_t>
	inline void insert_all_into_container(const new_container_t& new_container, container to_be_inserted, more_t ... more)
	{
		new_container.insert(new_container.end(),to_be_inserted.begin(),to_be_inserted.end());	
		insert_all_into_container(new_container,more...);
	}

	template<class container>
	inline container amalgamate(container first)
	{
		return first;
	}

	template<class container, class ... more_t>
	inline container amalgamate(container first, more_t ... more)
	{
		container new_container;
		insert_all_into_container(new_container,more...);
	}

	template<class T>
	inline std::vector<T> liquidate_vector(std::vector<std::vector<T>> in)
	{
		std::vector<T> out;
		for (std::vector<T>& container : in)
		{
			out.insert(out.begin(),container.begin(),container.end());
		}
		return out;
	}

	template<class T, class ID_t>
	inline std::vector<T> combine_similar_elements(std::vector<T> in, std::function<ID_t(const T&)> get_id, std::function<T(const T&, const T&)> combine_objects)
	{
		std::map<ID_t,size_t> ID_to_result_pos;
		std::vector<T> results;
		for (T object :in)
		{
			ID_t id = get_id(object);

			auto result_it = ID_to_result_pos.find(id);
			if (result_it == ID_to_result_pos.end())
			{
				results.push_back(object);
				ID_to_result_pos.insert(std::make_pair(id,std::distance(results.begin(),results.end())-1));
			}
			else
			{
				results[result_it->second] = combine_objects(results[result_it->second],object);
			}
		}
		return results;
	}

	template<class container, class return_t>
	inline std::vector<return_t> batch_function_calls(container& T_container, std::function<return_t(typename container::value_type&)> func)
	{
		std::vector<return_t> out;
		std::for_each(std::begin(T_container),std::end(T_container),[&out,func](typename container::value_type& object)
		{
			out.push_back(func(object));
		});
		return out;
	}

	template<class container, class return_t>
	inline std::vector<return_t> batch_function_calls(const container& T_container, std::function<return_t(const typename container::value_type&)> func)
	{
		std::vector<return_t> out;
		std::for_each(std::begin(T_container),std::end(T_container),[&out,func](typename container::value_type& object)
		{
			out.push_back(func(object));
		});
		return out;
	}

	template<template<typename> typename container, typename contained>
	inline container<std::reference_wrapper<contained>> convert_container_to_container_of_references_of_its_contained(container<contained> const & my_container)
	{
		container<std::reference_wrapper<contained>> new_container;
		for (auto& element : my_container)
		{
			new_container.push_back(element);
		}
		return new_container;
	}

	/*!
	 * Takes a container that contains pointers to a type and give a new container that contained std::reference_wrapper s to the type.
	 */
	template<template<typename> typename container, typename contained>
	inline auto convert_container_of_pointers_to_container_of_references_of_its_dereferenced(container<contained> const & my_container)
	{
		using dereferenced_t = dereference<contained>;
		container<std::reference_wrapper<dereferenced_t>> new_container;
		for (auto& element : my_container)
		{
			new_container.emplace_back(*element);
		}
		return new_container;
	}
}
