/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Misc. algorithms
 */
#pragma once

namespace algorithm
{

	template<typename T, typename Y>
	constexpr bool contains_any_flag(T bitfield, Y flag)
	{
		return (bitfield & flag) == flag;
	}
	template<typename T, typename Y, typename ... more_t>
	constexpr bool contains_any_flag(T bitfield, Y flag, more_t ... more)
	{
		return contains_any_flag(bitfield,flag) || contains_any_flag(bitfield, more...);
	}

}
