/*!
 * Framerate utilities
 */
#pragma once

#include <chrono>

namespace algorithm
{
	class only_linear_hysteresis_framerate_monitor
	{
		//TODO:
		//This will look at how long we took to compute the last frame, and use some 
		//coeffecient to enlongate that time, which will then be the predicted
		//time taken for next frame to be made.
	};
	class framerate_guard
	{
		bool moved = false;
		std::chrono::nanoseconds ideal_frame_time;
		std::chrono::time_point<std::chrono::steady_clock> start_time;
		public:
			inline framerate_guard(std::chrono::nanoseconds time) :
				ideal_frame_time(time),
				start_time(std::chrono::steady_clock::now())
			{
			}
			inline ~framerate_guard()
			{
				if (moved)
					return;
				//TODO:
				//	Take the time we exceeded the framerate by, and use the template 
				//	argressiveness value to calculate the sleep for. That way, we have a percentage
				//	that this catches up to its expected value.
				//
				//	Therefore, we have a percentage template, which we have in our render thread
				//	as a "how quickly" this tries to catch up with target framerate. Since it'd rather
				//	spread that out on VRR. Or on VSync.
				//	
				//	So we'd tear but only if you dropped frames, and aren't on Freesync.
				//		Speaking of, freesync compatibility(best experince) needs to be assured with this solution.
				//
				//	The time which we exceeded or preceded our target "pin" is given to us by a function provided
				//	by the creator of the guard.
				//		For graphics frame rate, this is given by the monitor's VSYNC, or by the physics engine's SYNC iff VRR.
				//			Our calculation of where to place it might be different, I'm not sure, but those are the variables it bases itself off of and constants (known when generating the function).
				//
				//
				//
				//	We are basically using our calculated "spare time" to try to keep in sync with physics.
				//	 	That way rendering starts right after physics has prepared some data.
				auto finish_time = std::chrono::steady_clock::now();
				auto time_took_to_tick = finish_time - start_time;
				std::this_thread::sleep_for( ideal_frame_time - time_took_to_tick);
			}
			framerate_guard(framerate_guard const &) = delete;
			framerate_guard(framerate_guard && o) :
				ideal_frame_time(o.ideal_frame_time),
				start_time(o.start_time)
			{
				o.moved = true;
			}
			framerate_guard& operator=(framerate_guard && o)
			{
				ideal_frame_time = o.ideal_frame_time;
				start_time = o.start_time;
				o.moved = true;
				return *this;
			}
	};
}
