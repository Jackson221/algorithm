/*!
 * Provides a pool of threads
 *
 * Copyright 2022 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <cassert>
#include <barrier>
#include <semaphore>
#include <future>

namespace algorithm
{
	class null_mutex
	{	
		public:
			inline null_mutex() {}
			null_mutex(null_mutex const &) = delete;
			null_mutex(null_mutex &&) = delete;
			inline void lock() {}
			inline void unlock() {}
			inline bool try_lock() { return true;}
	};
	template<typename T, bool do_recreate = false, bool always_work = false>
	/*!
	 * Pools together threads which work on things.
	 *
	 *
	 * This should be the LAST definition in your class!! Otherwise you will have race conditions on destruction.
	 * Basically, you need to keep in mind that this must be destroyed before any dependencies.
	 *
	 * If you depend on another thread pool, then you should be defined after it.
	 */
	class thread_pool //worker_pool? workers? easy_workers?
	{
		std::atomic<bool> alive = true;
		std::vector<std::thread> threads;
		std::function<T(size_t i)> initial_creator;
		std::counting_semaphore<999> work_sema; //used only once when always_work==true
		std::binary_semaphore stop_sema;
		size_t functions_finished = 0;
		size_t functions;
		std::barrier<std::function<void()>> sync_point;
		std::promise<void> finished_work_promise;

		std::promise<void> brake_promise;
		std::promise<void> brake_stop_promise;
		std::promise<void> released_promise;
		std::atomic<bool> brake_bool = false;

		bool threads_have_been_started = false;

		void do_brake_routine()
		{
			released_promise = std::promise<void>();
			brake_stop_promise = std::promise<void>();
			brake_promise.set_value();
			brake_promise = std::promise<void>();
			brake_stop_promise.get_future().wait();
			released_promise.set_value();
		}

		std::atomic<size_t> dead_threads=0;
		public:
			std::vector<T> finished_work;

			//Cannot brake before releasing brake and waiting on promise
			std::future<void> brake()
				requires(always_work)
			{
				assert(!brake_bool);
				auto fut = brake_promise.get_future();
				brake_bool = true;
				return fut;
			}
			//Cannot release brake before waiting on brake promise.
			std::future<void> release_brake()
				requires(always_work)
			{
				brake_bool = false;
				brake_stop_promise.set_value();
				return released_promise.get_future();
			}

			using worker_functions_t = std::function<void(T& data, size_t i)>;
			thread_pool
			(
			 	std::vector<worker_functions_t> worker_functions,
				size_t count = std::thread::hardware_concurrency(),
				std::function<T(size_t i)> initial_creator = [](size_t){return T{};},
				std::function<void(std::vector<T>& data, size_t functions_finished)> done_function = [](std::vector<T>&){}
			) :
				initial_creator(initial_creator),
				work_sema(0),
				stop_sema(0),
				functions(worker_functions.size()),
				sync_point(count, [=,this]() noexcept
				{
					if(alive)
					{
						done_function(finished_work,functions_finished);
						++functions_finished %= functions+1;
						if constexpr(!always_work)
						{
							if (functions_finished == 0)
							{
								finished_work_promise.set_value();
								work_sema.acquire();
							}
						}

						if (brake_bool)
							do_brake_routine();
					}
				})
			{
				finished_work.reserve(count);
				threads.reserve(count);
				for (size_t i = 0; i < count; i++)
				{
					finished_work.emplace_back(initial_creator(i));
					threads.emplace_back([this,i,worker_functions,done_function]()
					{
						work_sema.acquire();
						while(alive)
						{
							//do the function, and if last, then set the promise
							sync_point.arrive_and_wait(); //give a sync point before execution
							for (auto& worker_function : worker_functions)
							{
								worker_function(finished_work[i],i);
								sync_point.arrive_and_wait();
								if (!alive)
									break;
							}
						}
						if(++dead_threads == threads.size())
							stop_sema.release();
					});
					threads.back().detach();
				}
			}
			thread_pool
			(
			 	worker_functions_t worker_function,
				size_t count = std::thread::hardware_concurrency(),
				std::function<T(size_t i)> initial_creator = [](size_t){return T{};},
				std::function<void(std::vector<T>& data, size_t functions_finished)> done_function = [](std::vector<T>&,size_t){}
			) :
				thread_pool(std::vector{worker_function},count,initial_creator,done_function)
			{}
			thread_pool(thread_pool const &) = delete;
			thread_pool(thread_pool &&) = delete;
			~thread_pool()
			{ 
				alive = false; 
				if constexpr(always_work)
					if (brake_bool)
						release_brake().wait();
				if constexpr(!always_work)
					if (threads_have_been_started)
						work_sema.release(finished_work.size());
				if (threads_have_been_started)
					stop_sema.acquire();
			}
			void recreate()
			{
				size_t i = 0;
				for (T& w : finished_work)
					w = initial_creator(i++);
			}
			//Calling more than one time is malformed
			void start()
				requires(always_work)
			{
				work_sema.release(finished_work.size());
				threads_have_been_started = true;
			}

			//Cannot re-call before future is done.
			std::future<void> work()
				requires(!always_work)
			{
				if (!threads_have_been_started)
				{
					work_sema.release(finished_work.size());
					threads_have_been_started = true;
				}
				else
				{
					if constexpr(do_recreate)
						recreate();
					finished_work_promise = std::promise<void>();
					work_sema.release(finished_work.size());
				}
				return finished_work_promise.get_future();
			}
	};

	inline std::pair<size_t,size_t> this_thread_work_range(size_t nthreads, size_t work, size_t tnum)
	{
		size_t w_p_t = work / nthreads;
		//give the last thread slightly more work
		return std::make_pair(w_p_t * tnum, tnum+1 == nthreads? work : w_p_t*(tnum+1));
	}

	//Clever use of automatic 1-statement scope around if as well as inhereted block as first statement
	
	//Tries to lock a mutex, and if successful, then the next scope will be executed.
#define ALGORITHM_THREAD_TRY_LOCK_GUARD(mutex) if (mutex.try_lock()) if (auto ALGORITHM_TMP_ ##mutex = std::lock_guard(mutex, std::adopt_lock); true)
	


	//TODO: Jobs queue, with the job class using the exact same interface as thread_pool but with an additional constructor arg: priority (and maybe dependency chain off other jobs?)
	//Internally job_manager will have a thread_pool of nproc, which it will run the scheduler.
	//Thread count will virtually always be nproc when passed, but could potentially not be.
}
