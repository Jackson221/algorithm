#include "external/glm.hpp"

namespace algorithm
{
	const glm::quat empty_normal_quat = glm::normalize(glm::quat(glm::vec3(0,0,0)));
	inline glm::quat get_key_quat(glm::vec3 key)
	{
		glm::quat pitch = glm::quat(glm::vec3(key.x,0,0));
		glm::quat yaw = glm::quat(glm::vec3(0,key.y,0));
		glm::quat roll = glm::quat(glm::vec3(0,0,key.z));

		glm::quat key_quat = glm::normalize(pitch*empty_normal_quat);
		key_quat = glm::normalize(yaw*key_quat);
		key_quat = glm::normalize(roll*key_quat);

		return key_quat;
	}


}
