/*!
 * Types for making ECS systems easier
 *
 * Not super quality, still thinking of better ways of doing this
 */
#pragma once

#include <algorithm>
#include <unordered_map>
#include <utility>
#include <tuple>

namespace algorithm
{
	template<typename _object_t>
	struct ecs_container
	{
		using object_t = _object_t;
		size_t& next_id;
		std::unordered_map<size_t, object_t> map;

		ecs_container(size_t& next_id_ref) : next_id(next_id_ref)
		{
		}

		size_t push_back(object_t && obj)
		{
			map.insert(std::make_pair(next_id,std::move(obj)));
			return next_id++;
		}
		size_t push_back(object_t && obj, size_t id)
		{
			map.insert(std::make_pair(id, std::move(obj)));
			return id;
		}

		_object_t& operator[](size_t id)
		{
			return map.at(id);
		}

		void erase(size_t id)
		{
			map.erase(id);
		}
	};

	const size_t NO_OBJECT = std::numeric_limits<size_t>::max();

	template<typename object_t>
	class ecs_object
	{
		using container_t = ecs_container<object_t>;
		container_t* container;


		public:
			size_t id;

			ecs_object(container_t& container_ref, object_t&& object, size_t id) :
				container(&container_ref),
				id(id)
			{
				container_ref.push_back(std::move(object),id);
			}

			ecs_object(container_t& container_ref, size_t id) :
				container(&container_ref),
				id(id)
			{
			}

			ecs_object() :
				id(NO_OBJECT)
			{
			}


			ecs_object(ecs_object && other) :
				container(std::move(other.container)),
				id(std::exchange(other.id,NO_OBJECT))
			{
				assert(other.id == NO_OBJECT);
			}
			ecs_object(ecs_object const &) = delete;

			ecs_object& operator=(ecs_object && other)
			{
				container = other.container;
				id = std::exchange(other.id,NO_OBJECT);
				assert(other.id == NO_OBJECT);
				return *this;
			}
			ecs_object& operator=(std::tuple<container_t*, size_t> in)
			{
				container=  std::get<0>(in);
				id = std::get<1>(in);

				return *this;
			}
			ecs_object& operator=(ecs_object const &) = delete;

			~ecs_object()
			{
				if (id != NO_OBJECT)
				{
					container->erase(id);
				}
			}

			typename container_t::object_t& operator*()
			{
				return (*container)[id];
			}
			typename container_t::object_t& get()
			{
				return *(*this);
			}
			typename container_t::object_t* operator->()
			{
				return &((*container)[id]);
			}
	};


	template <typename member_ptr_t, typename func_t>
	struct ecs_container_descriptor
	{
		member_ptr_t member_ptr;
		func_t func;
	};

	//basically, construct an object(made of its components) using inputs_tuple_t
	//tuple is a compile-time tuple by using value_proxy on ecs_container_descriptor_tuple for the type list.
	template<typename ecs_container_descriptor_tuple, typename inputs_tuple_t>
	class ecs_objects_group
	{
		public:
			using populate_callback_t = std::function<void(size_t, inputs_tuple_t)>;
			
			using zeroth_member_ptr = decltype((std::tuple_element_t<0,ecs_container_descriptor_tuple>::value).member_ptr);

			using object_components_t = algorithm::get_class_of_member_pointer_t<zeroth_member_ptr>;
		//private:
			object_components_t* host;
			size_t id = NO_OBJECT;
		public:
			ecs_objects_group() {}
			ecs_objects_group(object_components_t& components_ref, inputs_tuple_t inputs_tuple) : 
				host(&components_ref)
			{
				id = components_ref.next_object_id++;
				variadic_util::for_each_type_in_variadic<ecs_container_descriptor_tuple>([&]<typename descriptor_proxy>()
				{
					auto descriptor = descriptor_proxy::value;
					(components_ref.*descriptor.member_ptr).push_back(std::apply(descriptor.func,inputs_tuple),id);
				});
			}
			~ecs_objects_group()
			{
				if (id != NO_OBJECT)
					variadic_util::for_each_type_in_variadic<ecs_container_descriptor_tuple>([&]<typename descriptor_proxy>()
					{
						auto descriptor = descriptor_proxy::value;
						(host->*descriptor.member_ptr).erase(id);
					});
			}

			ecs_objects_group(ecs_objects_group && other) :
				host(other.host),
				id(std::exchange(other.id,NO_OBJECT))
			{
				assert(other.id == NO_OBJECT);
			}
			ecs_objects_group(ecs_objects_group const &) = delete;

			ecs_objects_group& operator=(ecs_objects_group && other)
			{
				host = other.host;
				id = std::exchange(other.id,NO_OBJECT);
				return *this;
			}
			ecs_objects_group& operator=(ecs_objects_group const &) = delete;


			template<typename T>
			auto& operator[](T object_components_t::* ptr)
			{
				return ((*host).*ptr)[id];
			}
	};
}
