/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Type traits for things not provided by c++'s stl type traits
 */
#pragma once

#include <type_traits>
#include <utility>

namespace algorithm
{

	/*!
	 * This one's a dousy. Basically,I use _isnt_ambigous_with_a_member_function_specialization to determine whether a it would generate an ambigous function call if specializations are provided for both a. anybody and b. a class with this specific member function. 
	 *
	 * So, when it comes time to check if the members function exists, it must be ambigous if we call _isnt_ambigous_with_a_member_function_specialization. If it isn't, return false. Otherwise, this specialization fails, and then we add another specialization for if the member function exists. 
	 *
	 * Essentially, it's a round-about way(because of C++'s rules) of saying "one function for if there's a member class, and another for if there isn't". 
	 *
	 * Does not work on virtual member functions.
	 */
#define generate_check_function_for_member_function(member_function)\
template<typename T> constexpr std::enable_if_t<std::is_member_function_pointer_v<decltype(&T:: member_function )>,bool> member_function ## _isnt_ambigous_with_a_member_function_specialization();\
template<typename> constexpr bool member_function##_isnt_ambigous_with_a_member_function_specialization() { return true;}\
template<typename T> constexpr std::enable_if_t<member_function##_isnt_ambigous_with_a_member_function_specialization<T>(),bool> has_member_function_ ## member_function () { return false;}\
template<typename T> constexpr std::enable_if_t<std::is_member_function_pointer_v<decltype(&T:: member_function )>,bool> has_member_function_##member_function () {return true;}


	/*
	 * Okay, so it's been some time since I wrote the above but I want to say something...
	 * This is a logical error *almost*. When we specify one function, A, for if there is <thing> 
	 * and one function, A (overloaded), for if there is no A.
	 *
	 * A C++ rule is that substitution must result in one and only one function chosen. 
	 * (0) A is defined as existing if there's <thing>
	 * (1) A is also defined as existing if there's no A.
	 *
	 * And actually, this is fair, because according to this, there's always one and only one A.
	 *    But if you think about it, (1) A shouldn't exist. Because if A exists, then there is
	 *    an A so it isn't defined. But then A doesn't exist, so there is an A because of (1).
	 *    And so on and so fourth.
	 * But C++ has procedural grammar, so in actuality (1) is "if there isn't an A _already_"
	 */

	//some generations from the above
	//generate_check_function_for_member_function(has_value)
	//generate_check_function_for_member_function(size)
	
	template<typename T>
	concept has_size_member_fn = requires(T t)
	{
		{t.size()} -> std::same_as<size_t>;
	};
	template<typename T>
	concept not_has_size_member_fn = !has_size_member_fn<T>;


	template<typename T>
	struct type_proxy 
	{
		using type = T;
	};


	namespace
	{
		template<typename T>
		auto _internal_dereference(const T& y)
		{
			return std::move(*y); //use std::move to avoid causing problems for types which do not have a copy constructor.
		}
	}

	/*!
	 * Gives the result of the dereference operator. Distinguished from std::remove_reference because this works with types which overload the derefence operator such as std::shared_ptr and std::unique_ptr.
	 */
	template <typename T>
	using dereference = typename std::invoke_result_t<decltype(&_internal_dereference<T>),T>;

	template<typename T>
	struct remove_member_pointer
	{
		using type = T;
	};

	template<typename C, typename T>
	struct remove_member_pointer<T C::*>
	{
		using type = T;
		using class_t = C;
	};

	template<typename C, typename T>
	struct remove_member_pointer<T const C::*>
	{
		using type = T const;
		using class_t = C;
	};

	template<typename T>
	using remove_member_pointer_t = typename remove_member_pointer<T>::type;
	template<typename T>
	using get_class_of_member_pointer_t = typename remove_member_pointer<T>::class_t;

	template<typename T>
	using remove_const_member_pointer_t = remove_member_pointer_t<std::remove_const_t<T>>;
	template<typename T>
	using get_class_of_const_member_pointer_t = get_class_of_member_pointer_t<std::remove_const_t<T>>;

	template<typename T>
	concept MemberPointer = !std::is_same_v<T, algorithm::remove_const_member_pointer_t<T>>;

	template<typename T>
	struct remove_template_instantiation
	{
		using type = T;
	};
	template<typename T, template<typename> typename _>
	struct remove_template_instantiation<_<T>>
	{
		using type = T;
	};
	template<typename T>
	using remove_template_instantiation_t = typename remove_template_instantiation<T>::type;


	template<typename T>
	struct remove_const_qualifier
	{
		using type = T;
	};
	template<typename return_t, typename ... args>
	struct remove_const_qualifier<return_t(args...) const>
	{
		using type = return_t(args...);
	};
	template<typename return_t, typename ... args>
	struct remove_const_qualifier<return_t(args...)>
	{
		using type = return_t(args...);
	};

	template<typename T>
	using remove_const_qualifier_t = typename remove_const_qualifier<T>::type;




	///


	template<typename T>
	concept LambdaLike = requires(T t)
	{
		//simply assert that it is possible to create a member function ptr to operator()
		//
		//thus, this is only possible if operator() is defined in some fashion.
		&T::operator();
	};

	template<typename T, typename ... >
	struct impl_func_inputs_to_tuple_stage0 {};

	template<typename _return_t, typename ... args_t>
	struct impl_func_inputs_to_tuple_stage0<_return_t(args_t...)>
	{
		using type = std::tuple<args_t...>;
		using return_t = _return_t;
	};

	template<typename _func_t>
	auto impl_func_inputs_to_tuple_stage1()
	{
		using func_t = remove_const_qualifier_t<remove_const_member_pointer_t<_func_t>>;
		if constexpr(std::is_function_v<func_t>)
		{
			return type_proxy<impl_func_inputs_to_tuple_stage0<func_t>>{};
		}
		if constexpr(LambdaLike<func_t>)
		{
			return type_proxy<impl_func_inputs_to_tuple_stage0<remove_const_member_pointer_t<decltype(&func_t::operator())>>>{};
		}
	}



	template<typename func_t>
	using func_inputs_to_tuple_t = typename decltype(impl_func_inputs_to_tuple_stage1<func_t>())::type::type;

	template<typename func_t>
	using func_return_t = typename decltype(impl_func_inputs_to_tuple_stage1<func_t>())::type::return_t;


}
