# Algorithm module

This contains various utilies and helper functions that don't fit into any specific module.

# Dependancies

glm.hpp depends on glm

# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
